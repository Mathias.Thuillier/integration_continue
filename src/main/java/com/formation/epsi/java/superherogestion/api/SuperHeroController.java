package com.formation.epsi.java.superherogestion.api;

import com.formation.epsi.java.superherogestion.api.dto.SuperHeroDTO;
import com.formation.epsi.java.superherogestion.model.SuperHero;
import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
import net.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        path = "superHeroes",
        //consumes = {MediaType.APPLICATION_JSON_VALUE},
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class SuperHeroController {
   private final SuperHeroRepository superHeroRepository;

    public SuperHeroController(SuperHeroRepository superHeroRepository) {
        this.superHeroRepository = superHeroRepository;
    }

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAll(){
       /*List<SuperHero> superHeroes = this.superHeroRepository.findAll();
       List<SuperHeroDTO> superHeroDTO = new ArrayList<>();
       superHeroes.forEach(superHero -> {
           SuperHeroDTO superHeroDTOS = new SuperHeroDTO();
           superHeroDTOS.setNumber(superHero.getNumber());
           superHeroDTOS.setSecretIdentity(superHero.getSecretIdentity());
           superHeroDTOS.setSuperHeroName(superHero.getSuperHeroName());
           superHeroDTO.add(superHeroDTOS);
       });
       return ResponseEntity.ok(superHeroDTO);*/
        return ResponseEntity.ok(
                this.superHeroRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(path = "{number}")
    public ResponseEntity<SuperHeroDTO> getById(@PathVariable Integer number){
        Optional<SuperHero> optionalSuperHero = this.superHeroRepository.findById(number);
        if(optionalSuperHero.isPresent()){
            SuperHero superHero = optionalSuperHero.get();
            SuperHeroDTO superHeroDTO = mapToDTO(superHero);
            return ResponseEntity.ok(superHeroDTO);
        }else{
            return ResponseEntity.notFound().build();
        }


       /* return this.superHeroRepository.findById(number)
                .map(superHero -> ResponseEntity.ok(mapToDTO.superHero))
                .orElseGet(() -> ResponseEntity.notFound().build());*/
    }
    @PutMapping(path = "{number}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<SuperHeroDTO> update(
            @PathVariable Integer number,
            @RequestBody SuperHeroDTO superHeroDTO
    ){
        if(this.superHeroRepository.findById(number).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        SuperHero superHeroToUpdate = mapToEntity(superHeroDTO);
        return ResponseEntity.ok(mapToDTO(this.superHeroRepository.save(superHeroToUpdate)));
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<SuperHeroDTO> create(@RequestBody SuperHeroDTO superHeroDTO){
        SuperHero superHero = mapToEntity(superHeroDTO);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToDTO(this.superHeroRepository.save(superHero)));
    }

    @DeleteMapping(path = "{number")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer number){
        this.superHeroRepository.deleteById(number);
    }


    private SuperHero mapToEntity(SuperHeroDTO superHeroDTO){
        SuperHero superHero = new SuperHero();
        superHero.setNumber(superHeroDTO.getNumber());
        superHero.setSuperHeroName(superHeroDTO.getSuperHeroName());
        superHero.setSecretIdentity(superHeroDTO.getSecretIdentity());
        return superHero;
    }

    private SuperHeroDTO mapToDTO(SuperHero superHero)
    {
        return new SuperHeroDTO(
                superHero.getNumber(),
                superHero.getSuperHeroName(),
                superHero.getSecretIdentity()
        );
    }
}
