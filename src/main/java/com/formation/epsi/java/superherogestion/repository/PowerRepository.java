package com.formation.epsi.java.superherogestion.repository;

import com.formation.epsi.java.superherogestion.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PowerRepository extends JpaRepository<Power, String> {
    @Query("Select p from Power p where p.name =?1%")
    List<Power> findAllByNameIsStartingWith(String name);
}
