package com.formation.epsi.java.superherogestion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.formation.epsi.java.superherogestion.model.SuperHero;

public interface SuperHeroRepository extends JpaRepository<SuperHero, Integer>{

}
