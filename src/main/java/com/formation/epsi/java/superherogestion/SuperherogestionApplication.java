package com.formation.epsi.java.superherogestion;

import com.formation.epsi.java.superherogestion.model.Power;
import com.formation.epsi.java.superherogestion.model.SuperHero;
import com.formation.epsi.java.superherogestion.repository.PowerRepository;
import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SuperherogestionApplication {

	private final PowerRepository powerRepository;
	private final  SuperHeroRepository superHeroRepository;

	/*SuperherogestionApplication(  PAS BESOIN CAR IL Y A REQUIREDARGSCONSTRUCTOR
			PowerRepository powerRepository,
			SuperHeroRepository superHeroRepository
	){
		this.powerRepository = powerRepository;
		this.superHeroRepository = superHeroRepository;
	}*/

	public static void main(String[] args) {
		SpringApplication.run(SuperherogestionApplication.class, args);
	}


/*
	@PostConstruct
	public void initData(){
		//Liste de pouvoirs
		List<Power> power_list_superman = new ArrayList<Power>();

		//Pouvoir de Vol
		Power vol = new Power();
		vol.setName("Vol");
		vol.setDescription("Permet de voler");
		Power createdPower = powerRepository.save(vol);//Enregistrement dans la BDD
		System.out.println(createdPower.getName());//Visibilité de l'enregistrement
		power_list_superman.add(vol);//Ajout dans la BDD

		//Pouvoir d'invisibilité
		Power invisibilite = new Power();
		invisibilite.setName("Invisibilite");
		invisibilite.setDescription("Permet d'etre invisible");
		Power createPower2 = powerRepository.save(invisibilite);//Enregistrement dans la BDD
		power_list_superman.add(invisibilite);//Ajout dans la liste de pouvoirs


		//Super-Héro Superman
		SuperHero superman = new SuperHero();
		superman.setSuperHeroName("Superman");
		superman.setSecretIdentity("Clark Kent");
		SuperHero createSuperHero = superHeroRepository.save(superman);//Enregistrement dans la BDD
		superman.setPowers(power_list_superman);//Ajout de la liste des pouvoirs
		createSuperHero = superHeroRepository.save(superman);//Enregistrement dans la BDD
		System.out.println(createSuperHero.getSuperHeroName());
	}
	*/
}