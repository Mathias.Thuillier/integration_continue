package com.formation.epsi.java.superherogestion.api.dto;

import com.formation.epsi.java.superherogestion.model.SuperHero;
import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PowerDTO {
    private String name;
    private String description;
    private List<SuperHero> super_heros_list;
}
