package com.formation.epsi.java.superherogestion.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.implementation.bind.annotation.Super;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "vilain")
@Data
@NoArgsConstructor
public class Vilain {
    @Id
    private String name;

    @ManyToMany(mappedBy = "vilains")
    private List<SuperHero> super_heros_list;

    @OneToOne(mappedBy = "nemesis")
    private SuperHero nemesis;
}
