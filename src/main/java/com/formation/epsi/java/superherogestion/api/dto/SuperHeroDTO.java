package com.formation.epsi.java.superherogestion.api.dto;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SuperHeroDTO {
    private Integer number;
    private String superHeroName;
    private String secretIdentity;
/*
    public SuperHeroDTO(Integer number, String superHeroName, String secretIdentity) {
        this.number = number;
        this.superHeroName = superHeroName;
        this.secretIdentity = secretIdentity;
    }

    public SuperHeroDTO() {
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getSuperHeroName() {
        return superHeroName;
    }

    public void setSuperHeroName(String superHeroName) {
        this.superHeroName = superHeroName;
    }

    public String getSecretIdentity() {
        return secretIdentity;
    }

    public void setSecretIdentity(String secretIdentity) {
        this.secretIdentity = secretIdentity;
    }*/
}
