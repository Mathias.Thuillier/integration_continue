package com.formation.epsi.java.superherogestion.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.implementation.bind.annotation.Super;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="super_hero")
@Data
@NoArgsConstructor
public class SuperHero {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int number;
    @Column(name="super_hero_name", nullable=false)
    private String superHeroName;
    @Column(name="secret_identity", nullable = false)
    private String secretIdentity;

   @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
   @JoinTable(
           name = "super_heros_powers",
           joinColumns = {@JoinColumn(name = "super_hero_number")},
           inverseJoinColumns = {@JoinColumn(name = "power_name")}
   )
   List<Power> powers;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "super_heros_vilains",
            joinColumns = {@JoinColumn(name = "super_hero_number")},
            inverseJoinColumns = {@JoinColumn(name = "vilain_name")}
    )
    List<Vilain> vilains;

    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "nemesis_id", referencedColumnName = "name")
    private Vilain nemesis;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mentor_id")
    private SuperHero mentor;

    @OneToMany(mappedBy = "mentor", cascade = CascadeType.PERSIST)
    private List<SuperHero> sidekicks;

}
