package com.formation.epsi.java.superherogestion.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.bytebuddy.implementation.bind.annotation.Super;

import javax.persistence.*;
import javax.swing.plaf.ActionMapUIResource;
import java.text.CollationElementIterator;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="power")
@Data
@NoArgsConstructor
public class Power {
    @Id
    private String name;
    @Column(name="power_description", nullable = false)
    private String description;

    @ManyToMany(mappedBy = "powers")
    private List<SuperHero> super_heros_list;

}
