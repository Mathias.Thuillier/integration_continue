package com.formation.epsi.java.superherogestion.api;

import com.formation.epsi.java.superherogestion.api.dto.PowerDTO;
import com.formation.epsi.java.superherogestion.api.dto.SuperHeroDTO;
import com.formation.epsi.java.superherogestion.model.Power;
import com.formation.epsi.java.superherogestion.model.SuperHero;
import com.formation.epsi.java.superherogestion.repository.PowerRepository;
import com.formation.epsi.java.superherogestion.repository.SuperHeroRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        path = "powers",
        //consumes = {MediaType.APPLICATION_JSON_VALUE},
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class PowerController {
    private final PowerRepository powerRepository;

    public PowerController(PowerRepository powerRepository) {
        this.powerRepository = powerRepository;
    }

    @GetMapping
    public ResponseEntity<List<PowerDTO>> getAll(){
        return ResponseEntity.ok(
                this.powerRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(path = "search")
    public ResponseEntity<List<PowerDTO>> searchByName(@RequestParam(name = "name") String name){
        return ResponseEntity.ok(
                this.powerRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(path = "{name}")
    public ResponseEntity<PowerDTO> getById(@PathVariable String name){
        Optional<Power> optionalPower = this.powerRepository.findById(name);
        if(optionalPower.isPresent()){
            Power power = optionalPower.get();
            PowerDTO powerDTO = mapToDTO(power);
            return ResponseEntity.ok(powerDTO);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "{name}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<PowerDTO> update(
            @PathVariable String name,
            @RequestBody PowerDTO powerDTO
    ){
        if(this.powerRepository.findById(name).isEmpty()){
            return ResponseEntity.notFound().build();
        }
        Power powerToUpdate = mapToEntity(powerDTO);
        return ResponseEntity.ok(mapToDTO(this.powerRepository.save(powerToUpdate)));
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<PowerDTO> create(@RequestBody PowerDTO powerDTO){
        Power power = mapToEntity(powerDTO);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToDTO(this.powerRepository.save(power)));
    }

    @DeleteMapping(path = "{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String name){
        this.powerRepository.deleteById(name);
    }

    private Power mapToEntity(PowerDTO powerDTO){
        Power power = new Power();
        power.setName(powerDTO.getName());
        power.setDescription(powerDTO.getDescription());
        power.setSuper_heros_list(powerDTO.getSuper_heros_list());
        return power;
    }

    private PowerDTO mapToDTO(Power power)
    {
        return new PowerDTO(
                power.getName(),
                power.getDescription(),
                power.getSuper_heros_list()
        );
    }
}
